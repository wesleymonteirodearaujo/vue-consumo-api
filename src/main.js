import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'

const app = createApp(App);

// Configurar o Axios com a URL da sua API GraphQL
axios.defaults.baseURL = 'http://localhost:4000'; // Substitua pela URL correta

// Adicionar o Axios à instância do Vue
app.config.globalProperties.$axios = axios;

app.use(router).mount('#app');
