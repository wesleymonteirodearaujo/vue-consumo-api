// AlunoService.js

import axios from 'axios';

const BASE_URL = 'http://localhost:4000/graphql'; // Substitua pela URL correta da sua API GraphQL

export default {
  fetchAlunos() {
    return axios
      .post(BASE_URL, {
        query: `
          query {
            alunos {
              id
              nome
            }
          }
        `,
      })
      .then((response) => response.data.data.alunos)
      .catch((error) => {
        throw new Error('Erro ao buscar alunos: ' + error);
      });
  },
  createAluno(nome, id) {
    return axios
      .post(BASE_URL, {
        query: `
          mutation {
            createAluno(nome: "${nome}", id: "${id}") {
              id
              nome
            }
          }
        `,
      })
      .then((response) => response.data.data.createAluno)
      .catch((error) => {
        throw new Error('Erro ao criar aluno: ' + error);
      });
  },
};
